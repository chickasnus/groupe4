import React,{ useState } from 'react';


function Header({nextPage}){
        
    return (
      <div className="App">
        <header className="App-header">
          <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <a className="navbar-brand" href="#" onClick={() => nextPage('Home')}>Broken Simplon</a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>  
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
              <ul className="navbar-nav mr-auto">
                <li className="nav-item active">
                  <a className="nav-link" href="#" onClick={() => nextPage('Home')}>Home</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#" onClick={() => nextPage('Add Person')}>Add Person</a>
                </li>
              </ul>
            </div>
          </nav>  
        </header>     
      </div>
    );
  }

  export default Header;
  

